import scrollSpy from "simple-scrollspy";

class SidenavScrollspy extends HTMLElement {
  constructor() {
    super();
    scrollSpy(this, { offset: 10 });
  }
}

export default SidenavScrollspy;
