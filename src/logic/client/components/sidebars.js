import { SidebarElement, SidebarService } from "sidebarjs";

class SidebarJS extends HTMLElement {
  constructor() {
    super();

    this.classList.remove("d-none");
    let el = this;

    let sidebar = new SidebarElement({
      onOpen: function () {
        if (el.dataset.trackingname)
          _paq.push([
            "trackEvent",
            "Sidebar",
            el.dataset.trackingname,
            window.location.pathname
          ]);
      },
      component: this,
      position: "right",
      nativeSwipeOpen: false
    });
  }
}

export default SidebarJS;
