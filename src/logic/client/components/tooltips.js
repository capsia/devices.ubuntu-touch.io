import tippy from "tippy.js";

class TippyTooltip extends HTMLElement {
  constructor() {
    super();

    const target = document.querySelector(this.dataset.tippy);

    if (!!target) {
      this.classList.remove("d-none");
      tippy(target, {
        placement: this.dataset.placement || "auto",
        interactive: true,
        animation: "shift-away",
        trigger: this.dataset.trigger || "mouseenter focus",
        content: this
      });
    }
  }
}

export default TippyTooltip;
