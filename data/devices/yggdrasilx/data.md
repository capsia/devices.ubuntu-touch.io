---
name: "Volla Phone X"
deviceType: "phone"
image: "https://volla.online/de/resources/Store/Volla-Phone-X/volla-phone-x.png"
buyLink: "https://volla.online/de/shop/volla-phone-x/"
description: "The rugged Volla Phone X offers double security with its IP68 dust and water-proof casing and an uncompromising privacy respecting, open source operating system, combined with a unique user experience. The device has a 13 megapixel Sony camera with 2 megapixel macro lens, 64 GB internal memory, a powerful 8-core processor. The bright display and extremely long lasting 6200 mAh battery are perfect for outdoor activities. You can get the device pre-installed with Ubuntu Touch or as a 2nd operating system with the unique multi-boot feature of Volla OS."
tag: "promoted"
subforum: "100/volla-phone-x"
price:
  min: 269
  max: 449
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53 (4x 2.0 GHz + 4x 1.5 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P23, MT6763V"
  - id: "gpu"
    value: "ARM Mali-G71 MP2 @ 770 MHz, 2 cores"
  - id: "rom"
    value: "64 GB, eMMC"
  - id: "ram"
    value: "4 GB, DDR3"
  - id: "android"
    value: "10.0"
  - id: "battery"
    value: "6200 mAh, Li-Polymer"
  - id: "display"
    value: '6.1" IPS, 720 x 1560 (283 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "13MP (f/2.0, 1080p30 video) + 2MP (for bokeh/depth), PDAF, LED flash"
  - id: "frontCamera"
    value: "8MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "162.4 mm x 79 mm x 15.3 mm"
  - id: "weight"
    value: "280 g"
  - id: "releaseDate"
    value: "July 2021"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
    forum: "https://volla.online/en/"
    photo: "https://avatars.githubusercontent.com/u/62447194" # https://github.com/HelloVolla
  - name: "TheKit"
    role: "Developer"
    forum: "https://forums.ubports.com/user/thekit"
  - name: "Deathmist"
    role: "Developer"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-3171/3171-profileavatar-1613145487767.png"
  - name: "Lukapanio"
    role: "Tester"
    forum: "https://forums.ubports.com/user/lukapanio"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

sources:
  portType: "reference"
  portPath: "android10"
  deviceGroup: "volla-phone-x"
  deviceSource: "volla-yggdrasilx"
  kernelSource: "kernel-volla-mt6763"

externalLinks:
  - name: "UBports Installer config"
    link: "https://github.com/ubports/installer-configs/blob/master/v2/devices/yggdrasilx.yml"
  - name: "Source for details on this page"
    link: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices/yggdrasilx"

seo:
  description: "Get your Volla Phone with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhoneX, Volla Phone X, Privacy Phone"
---
