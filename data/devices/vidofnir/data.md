---
name: "Volla Phone X23"
deviceType: "phone"
image: "https://gitlab.com/design3680819/ut-hw-models/-/raw/main/Phones/Volla%20Phone%20X23/VollaPhone-X23.png"
buyLink: "https://volla.online/de/shop/volla-phone-x23/"
description: "The rugged Volla Phone X23 offers double security with its IP68 dust and water-proof casing and an uncompromising privacy respecting, open source operating system, combined with a unique user experience. The device has a 48 megapixel camera with 8 megapixel macro lens, 128 GB internal memory, a powerful 8-core processor. The bright display and long lasting 5000 mAh removable battery are perfect for outdoor activities. You can get the device pre-installed with Ubuntu Touch or as a 2nd operating system with the unique multi-boot feature of Volla OS."
tag: "promoted"
#subforum: "???/volla-phone-x23" # FIXME: who can make this?! not seen in https://forums.ubports.com/category/89/volla
price:
  avg: 538
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM (2x Cortex-A76 @ 2.2 GHz + 6x Cortex-A55 @ 2.0 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio G99, MT6789"
  - id: "gpu"
    value: "ARM Mali-G57 MC2 (Valhall) @ 850 MHz, 2 cores"
  - id: "rom"
    value: "128 GB, UFS 2.1"
  - id: "ram"
    value: "6 GB, LPDDR4X @ 2133 MHz"
  - id: "android"
    value: "12.0"
  - id: "battery"
    value: "5000 mAh, Li-Polymer, Removable"
  - id: "display"
    value: '6.1" IPS, 1560 x 720 (282 PPI), U-notch, Rounded corners'
  - id: "rearCamera"
    value: "48MP (f/1.79, 1440p30 video) + 8MP (f/2.2 ultra wide-angle / macro lens), PDAF, LED flash"
  - id: "frontCamera"
    value: "16MP (f/2.0)"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "160.9 mm x 80 mm x 12.2 mm"
  - id: "weight"
    value: "270 g"
  - id: "releaseDate"
    value: "May 2023"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
    forum: "https://volla.online/en/"
    photo: "https://avatars.githubusercontent.com/u/62447194" # https://github.com/HelloVolla
  - name: "TheKit"
    role: "Developer"
    forum: "https://forums.ubports.com/user/thekit"
  - name: "Deathmist"
    role: "Developer"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-3171/3171-profileavatar-1613145487767.png"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

sources:
  portType: "reference"
  portPath: "halium12"
  deviceGroup: "volla-x23"
  deviceSource: "volla-vidofnir"
  kernelSource: "kernel-volla-mt6789"

externalLinks:
  - name: "Pending UBports Installer config PR"
    link: "https://github.com/ubports/installer-configs/pull/240"

seo:
  description: "Get your Volla Phone X23 with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhoneX23, Volla Phone X23, Privacy Phone"
---
