---
portType: "Halium 11.0"
kernelVersion: "3.18.140"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
        bugTracker: "https://gitlab.com/ubports/development/core/qtubuntu-camera/-/issues/22"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+-"
        bugTracker: "https://gitlab.com/ubports/porting/community-ports/android11/samsung-galaxy-s7/samsung-exynos8890/-/issues/2"
      - id: "dualSim"
        value: "+"
      - id: "mms"
        value: "+-"
        bugTracker: "https://gitlab.com/ubports/porting/community-ports/android11/samsung-galaxy-s7/samsung-exynos8890/-/issues/1"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "?"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "?"
      - id: "waydroid"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "fmRadio"
        value: "x"
      - id: "hotspot"
        value: "+"
        overrideGlobal: true
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
        bugTracker: "https://gitlab.com/ubports/development/core/packaging/sensorfw/-/merge_requests/8"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
      - id: "dt2w"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

<section id="credits">

[Ivan Meler](https://github.com/ivanmeler) and other contributors of [8890q](https://github.com/8890q/) made this port possible with their LineageOS 18.1 porting effort

</section>

<section id="notes">

- Dual sim Galaxy S7 exynos can only use 3G/4G on one of the sim slot at a time. While it is possible on Android to pick a sim for 3G/4G, on this port, only sim1 can use 3G/4G, sim2 is 2G only.
- Fingerprint sensor drivers are patched to work with halium, but only tested on one of the possible sensor model. Create an issue if fingerprint sensor is not working for you.
- Battery charging limit can be set by writing an integer percentage value from 61-94 to `~/.config/limit_charging` then reboot, eg. `echo 70 > ~/.config/limit_charging; sudo reboot`
- While [Ivan Meler](https://github.com/ivanmeler) might still be attempting to [port 8895's 4.4 kernel onto 8890](https://forum.xda-developers.com/t/lineageos-19-1-android-12l-signature-spoofing-ota-updates-for-s7-exynos.4368995/page-25#post-86466539), this device runs on an extremely old downstream kernel. Though porting work seems mostly smooth on focal, odd systemd behaviors had to be worked around. If you're looking to buy a device to run Ubuntu Touch or other phone Linux distros, I advice looking for a device with a more recent kernel, or better, mainlined kernel. Focal will likely run unless some big changes happen, but I can't say for sure for future versions.
- If my device breaks down, I likely won't be able to continue maintaining this port unless I somehow receive a replacement.

</section>
