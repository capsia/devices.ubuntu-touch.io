---
name: "Google Pixel 3a"
comment: "community device"
description: "The Pixel 3a is available in Black, Clearly White, and Purple-ish. Armed with a Snapdragon 670 processor and 4GB of RAM, it has a good price for a Linux phone. What else? An adequate battery capacity of 3,000 mAh, vivid 5.6-inch OLED display, Type-C USB, headphone jack, and good camera quality."
deviceType: "phone"
image: "https://gitlab.com/design3680819/ut-hw-models/-/raw/main/Phones/Google%20Pixel%203a/Pixel3a.png"
tag: "promoted"
subforum: "69/google-pixel-3a-3a-xl"
price:
  avg: 100

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm SDM670 Snapdragon 670 (10 nm)"
  - id: "gpu"
    value: "Qualcomm Adreno 615"
  - id: "rom"
    value: "64GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "Android 9.0"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "1080x2220 pixels, 5.6 in"
  - id: "rearCamera"
    value: "12.2MP"
  - id: "frontCamera"
    value: "8MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "152.4mm x 71.1mm x 7.6mm"
  - id: "weight"
    value: "147 g"
  - id: "releaseDate"
    value: "May 2019"

sources:
  portType: "reference"
  portPath: "android9"
  deviceGroup: "google-pixel-3a"
  deviceSource: "android_device_google_bonito"
  kernelSource: "android_kernel_google_bonito"

communityHelp:
  - name: "Telegram - @ubports_pixel3a"
    link: "https://t.me/ubports_pixel3a"

contributors:
  - name: "fredldotme"
    role: "Maintainer"
    forum: "https://forums.ubports.com/user/fredldotme"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-2070/2070-profileavatar.png"
---
