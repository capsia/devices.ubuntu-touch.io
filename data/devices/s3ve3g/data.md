---
name: "Samsung S3 Neo+ (GT-I9301I)"
deviceType: "phone"
subforum: "62/samsung-s3-neo"

contributors:
  - name: "Flohack"
    forum: "https://forums.ubports.com/user/Flohack"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-414/414-profileavatar.png"
---
